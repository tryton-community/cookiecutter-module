.. This file is part of {{ cookiecutter.__packagename }}.
   Licensed under the GNU Free Documentation License v1.3 or any later version.
   The COPYRIGHT file at the top level of this repository contains the
   full copyright notices and license terms.
   SPDX-License-Identifier: GFDL-1.3-or-later

{{ '#' * (cookiecutter.module_name|replace('_', ' ')|title|length + 7) }}
{{ cookiecutter.module_name.replace('_', ' ').title() }} Module
{{ '#' * (cookiecutter.module_name|replace('_', ' ')|title|length + 7) }}

.. Please write some words about what this module does
   and for what purpose was it written.
   A single sentence is usually not sufficient.
