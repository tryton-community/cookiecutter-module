.. This file is part of {{ cookiecutter.__packagename }}.
   Licensed under the GNU Free Documentation License v1.3 or any later version.
   The COPYRIGHT file at the top level of this repository contains the
   full copyright notices and license terms.
   SPDX-License-Identifier: GFDL-1.3-or-later

*****
Usage
*****

.. For what to write into this file please read
   https://www.tryton.org/develop/guidelines/documentation#usage.rst
   and remove this comment afterwards.
