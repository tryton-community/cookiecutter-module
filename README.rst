===================
cookiecutter-module
===================

Cookiecutter_ template for a Tryton Community module.

Using Cookiecutter and this template
you can set up a new project for a new Tryton module quickly.

This template is based in the official template from Tryton,
albeit adjusted to Tryton Community and enhanced.


Features
--------

* Support for different versions of Tryton
* Ready to use `setup.py`, using `trytoncommunity-setuptools`
* Vanilla testing setup with `unittest`
* Tox_: Setup to easily test for different versions of Python
  as well as SQlite and PostgreSQL
* Easy linting/code-checking via ``tox -e lint``
* Documentation folder with standard layout
* Copyright header in all relevant files
* Elaborated and detailed Copyright file
* Integration for Gitlab/Heptapos CI/CD,
  which features alaborated test
  as well as deployment to Heptapod/Gitlab and PyPi.
  See the `Tryton Community CI-Tools`__ for deails.

__ https://foss.heptapod.net/tryton-community/community/ci-tools


Quickstart
----------

Install `cookiecutter`::

  pip install --user --upgrade cookiecutter

Create a new Tryton module based on the latest template
in the current directory,
without downloading this repo::

  cookiecutter \
     https://foss.heptapod.net/tryton-community/cookiecutter-module.git


Advanced Usage
--------------

* To create the new Tryton module in ``/path/to/your/modules`` run::

    cookiecutter --output-dir /path/to/your/modules \
       https://foss.heptapod.net/tryton-community/cookiecutter-module.git

For more options please refer to ``cookicutter --help`` and
the Cookiecutter_ website.


Enhancing this Template
-----------------------

Your enhancements to this template are very welcome.
To test your changes proceed along these lines::

  cd /path/to/the/checkout/of/this/repo
  cookiecutter --output-dir /tmp/test-modules .


.. _Cookiecutter: https://cookiecutter.readthedocs.io/
.. _Tox: https://tox.wiki/
