import os
import shutil

for fn in ('_gitignore', '_hgignore'):
    shutil.move(fn, '.' + fn[1:])
